const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
 exports.helloWorld = functions.https.onRequest((request, response) => {
  response.send("Initializing Database");
 });

 exports.getWhat = functions.https.onRequest((req, res) => {
    admin.firestore().collection('what').get()
        .then(data => {
            let what = [];
            data.forEach(doc => {
                what.push(doc.data());
            });
            return res.json(what);  
        })
        .catch(err => console.error(err));
    })
